# OptimizedStringOperation

Concatenating many strings allocates a lot of temporary memory in the managed heap.
It is recommend to use System.Text.StringBuffer instead.

Sometimes this is hard to fix, especially if you already have written a lot of code.
This solution should help you to work around that problem easily.

To initialise the system just put following line of code into your script:
```csharp
using StrOpe = StringOperationUtil.OptimizedStringOperation;
```
For a line of code looking like that:
```csharp
string str = "aaa" + 20 + "bbbb"; 
```
Just add the StrOpe.i in front of the string concatination:  
```csharp
string str = StrOpe.i + "aaa" + 20 + "bbbb"; 
```

Internally StrOpe.i uses the StringBuilder class. The system implements it by overriding the + operator and implicit casts.

StrOpe.i reuses same object over and over again. Additionally you can use StrOpe.small / StrOpe.medium / StrOpe.large. These are creating an separate instance so that it is thread safe

# Test Scene

To run the demo just follow following steps:

* Open sample.scene and run.
* By clikcing the screen you can change the usage of StrOpe.i.
* sbt Flag true signals that the program uses the faster path.

The result you will see will be similar to this:

* Execute Time   : 275 msec => 3.5msec
* Managed memory : 382.2MB -> 360B 

The executed code is: 
```csharp
string str = "TestD";
for (int i = 0; i < 20000; ++i){
    str += "a";
}
```