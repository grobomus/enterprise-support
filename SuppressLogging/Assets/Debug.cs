﻿#define LOGGING_CONDITION

using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;

public static class Debug
{

	[Conditional("LOGGING_CONDITION")]
	public static void Log(string message, UnityEngine.Object obj = null)
	{
		UnityEngine.Debug.Log(message, obj);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogWarning(string message, UnityEngine.Object obj = null)
	{
		UnityEngine.Debug.LogWarning(message, obj);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogError(string message, UnityEngine.Object obj = null)
	{
		UnityEngine.Debug.LogError(message, obj);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogException(Exception exception)
	{
		UnityEngine.Debug.LogException(exception);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogException(Exception exception, UnityEngine.Object context)
	{
		UnityEngine.Debug.LogException(exception, context);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogErrorFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogErrorFormat(message, args);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogFormat(message, args);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void LogWarningFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogWarningFormat(message, args);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void Assert(bool condition)
	{
		UnityEngine.Debug.Assert(condition);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void Assert(bool condition, UnityEngine.Object context)
	{
		UnityEngine.Debug.Assert(condition, context);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void Assert(bool condition, object message)
	{
		UnityEngine.Debug.Assert(condition, message);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void Assert(bool condition, object message, UnityEngine.Object context)
	{
		UnityEngine.Debug.Assert(condition, message, context);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void AssertFormat(bool condition, string format, params object[] args)
	{
		UnityEngine.Debug.AssertFormat(condition, format, args);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void DrawLine(Vector3 start, Vector3 end, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
	{
		UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
	}

	[Conditional("LOGGING_CONDITION")]
	public static void DrawRay(Vector3 start, Vector3 dir, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
	{
		UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest);
	}

	// Simulate release build.
#if LOGGING_CONDITION
	public static bool isDebugBuild = true;

#else
	public static bool isDebugBuild = false;
#endif
}
